import React from 'react';
import styles from "./Heading.module.css"

function Heading() {
  return(
    <div className={styles.HeadingContainer}>
      <h2>Tasks</h2>
    </div>
  );
}

export default Heading;
