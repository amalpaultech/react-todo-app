import React from 'react';
import styles from "./Item.module.css"


function Item(props) {
  return (<div className={styles.Container}>
      <div className={styles.Item}>
          <p>{props.item}</p>
          </div>
      <div className={styles.DelBtn} onClick={props.deleteItem}><p>Delete</p></div>
  </div>);
}

export default Item;
