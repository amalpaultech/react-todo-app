import React, { useState } from 'react';
import Item from '../Item/Item';
import styles from "./ToDo.module.css"


function ToDo() {
  const [list, setList] = useState([])
  const [toDo, setToDo] = useState("")

  function generateId() {
    if (list.length === 0 )
      return 1
    return (Math.max(...list.map((item) => item.id)) + 1);
  }

  function createToDoItem() {
    if (toDo !== "")
      setList([...list, {id: generateId(),text:toDo}])
  }

  function handleClick () {
    createToDoItem()
    setToDo("")
  }

  function handleInput(event) {
    setToDo(event.target.value)

  }

  function deleteItem(id) {
    setList(list.filter((value,index) => {
      return value.id !== id
    }))
  }

  return (
    <div className={styles.Container}>
      {list.map((item) => {
        return <Item key={item.id} item={item.text} deleteItem={() => deleteItem(item.id)}/>;
      })}
      <div className={styles.NewContainer}>
        <div className={styles.Item}>
          <input type="text" value={toDo} onChange={handleInput}/>
        </div>
        <div className={styles.AddBtn} onClick={handleClick}><p>Add</p></div>
      </div>
    </div>);
}

export default ToDo;