import "./App.css"
import Heading from "./components/Heading/Heading";
import ToDo from "./components/ToDo/ToDo";

function App() {

  return (
    <div className="App">
      <div className="Base">
        <Heading />
        <ToDo />
      </div>
    </div>);
}

export default App; 